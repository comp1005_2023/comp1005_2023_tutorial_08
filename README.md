COMP1005 2022 Tutorial 8
========================


## Background

* [Sorting Techniques](https://www.tutorialspoint.com/data_structures_algorithms/sorting_algorithms.htm)
    * [Bubble Sort](https://www.tutorialspoint.com/data_structures_algorithms/bubble_sort_algorithm.htm)
    * [Quick Sort](https://www.tutorialspoint.com/data_structures_algorithms/quick_sort_algorithm.htm)
    * [Insertion Sort](https://www.tutorialspoint.com/data_structures_algorithms/insertion_sort_algorithm.htm)
    * [Merge Sort](https://www.tutorialspoint.com/data_structures_algorithms/merge_sort_algorithm.htm)
    * [Shell Sort](https://www.tutorialspoint.com/data_structures_algorithms/shell_sort_algorithm.htm)
* [Switch Statement](https://www.tutorialspoint.com/cprogramming/switch_statement_in_c.htm)
* [Expression Parsing](https://www.tutorialspoint.com/data_structures_algorithms/expression_parsing.htm)
    * [Shunting-yard algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm)


## Exercises
0. Write a C program to sort a list of elements using the insertion sort algorithm.  
    <img src="https://www.w3resource.com/w3r_images/insertion-sort.png" width="256">  

0. Write a C program to sort a list of elements using the merge sort algorithm.  
    <img src="https://www.w3resource.com/w3r_images/merge_sort.png" width="256">  

0. Try to understand the code [Expression Parsing Using Stack](https://www.tutorialspoint.com/data_structures_algorithms/expression_parsing_using_statck.htm).  

## Extras
0. Write a C program to sort a list of elements using the bubble sort algorithm.  
    <img src="https://www.w3resource.com/w3r_images/bubble-short.png" width="256">  

0. Write a C program to sort a list of elements using the quick sort algorithm.  
    <img src="https://www.w3resource.com/w3r_images/quick-sort-part-1.png" width="256">  
    <img src="https://www.w3resource.com/w3r_images/quick-sort-part-2.png" width="256">  

0. Try other sorting algorithms (see [WIKIPEDIA](https://en.wikipedia.org/wiki/Sorting_algorithm)).  

0. Try to implement the shunting-yard algorithm by yourself.  

0. Review the use of multi-dimensional Arrays and complex arrays.  
