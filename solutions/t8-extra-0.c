/*
 * https://www.w3resource.com/c-programming-exercises/searching-and-sorting/c-search-and-sorting-exercise-3.php
 */

#include <stdio.h>

/* function  declaration */
int find_max(int b[], int k);
void exchange(int b[], int k);

int main() {
    int arr[10];
    int i, N;

    printf("\nInput no. of values in the array: ");
    scanf("%d", &N);
    printf("\nInput the elements: \n");
    for (i = 0; i < N; i++) {
        scanf("%d", &arr[i]);
    }
    /* Selection sorting  begins */
    exchange(arr, N);
    printf("Sorted  array :\n");
    for (i = 0; i < N; i++) {
        printf("%d\n", arr[i]);
    }
    return 0;
}

/* function to find the maximum value */
int find_max(int b[], int k) {
    int max = 0, j;
    for (j = 1; j <= k; j++) {
        if (b[j] > b[max]) {
            max = j;
        }
    }
    return (max);
}

void exchange(int b[], int k) {
    int temp, big, j;
    for (j = k - 1; j >= 1; j--) {
        big = find_max(b, j);
        temp = b[big];
        b[big] = b[j];
        b[j] = temp;
    }
    return;
}
