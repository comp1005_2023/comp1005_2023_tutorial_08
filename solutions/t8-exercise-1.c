/*
 * https://www.w3resource.com/c-programming-exercises/searching-and-sorting/c-search-and-sorting-exercise-5.php
 */

#include <stdio.h>
#include <stdlib.h>

/* Function to merge the two haves array[l..m] and array[m+1..r] of array array[] */
void merge(int array[], int l, int m, int r) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;
    /* create temp arrays */
    int *L, *R;

    L = (int *) malloc(sizeof(int) * n1);
    R = (int *) malloc(sizeof(int) * n2);

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) {
        L[i] = array[l + i];
    }
    for (j = 0; j < n2; j++) {
        R[j] = array[m + 1 + j];
    }

    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            array[k] = L[i];
            i++;
        } else {
            array[k] = R[j];
            j++;
        }
        k++;
    }
    while (i < n1) {
        array[k] = L[i];
        i++;
        k++;
    }
    while (j < n2) {
        array[k] = R[j];
        j++;
        k++;
    }

    free(L);
    free(R);
}

void mergeSort(int array[], int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;    /*Same as (l+r)/2, but avoids overflow for large l and h*/
        mergeSort(array, l, m);
        mergeSort(array, m + 1, r);
        merge(array, l, m, r);
    }
}

/* Function to print an array */
void print_array(int A[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");
}

/* Test above functions */
int main() {
    int array[] = {125, 181, 130, 25, 61, 887};
    int arr_size = sizeof(array) / sizeof(array[0]);
    printf("Given array is \n");
    print_array(array, arr_size);
    mergeSort(array, 0, arr_size - 1);
    printf("\nSorted array is \n");
    print_array(array, arr_size);
    return 0;
}
