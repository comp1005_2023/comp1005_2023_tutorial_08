/*
 * https://www.w3resource.com/c-programming-exercises/searching-and-sorting/c-search-and-sorting-exercise-4.php
 */

#include <stdio.h>

int main() {
    int array[10], i, j, n, array_key;
    printf("Input  no. of values in the array: \n");
    scanf("%d", &n);
    printf("Input  %d array value(s): \n", n);
    for (i = 0; i < n; i++) {
        scanf("%d", &array[i]);
    }

    /* Insertion Sort  */
    for (i = 1; i < n; i++) {
        array_key = array[i];
        j = i - 1;

        while (j >= 0 && array[j] > array_key) {
            array[j + 1] = array[j];
            j = j - 1;
        }
        array[j + 1] = array_key;
    }

    printf("Sorted  Array: \n");
    for (i = 0; i < n; i++) {
        printf("%d  \n", array[i]);
    }

    return 0;
}
